# Backlog du sprint 1

## User stories to do :
### Design boitier de connexion

[x] Etude d'aptabilité des connectiques entre Android et IOS  
[x] Modélisation du boitier sur Solidworks  

### Antenne de communication entre smartphone et drone

[x] Réalisation d'un état de l'art sur les antennes pour drone  
[x] Choix du type d'antenne  

### IHM de pilotage en application smartphone

[x] Lister les fonctionnalitées que l'IHM doit fournir  
[x] Etude de l'ergonomie et du design de l'IHM  
[x] Réalisation de maquette d'IHM  


### Le projet reste stable à chaques étapes (test driven dev)
### L'quipe gère optimalement son temps (pomodoro)
### Les dev minimize leur erreurs (revue de code + pair programming)