# Backlog du produit

description 
Flyphone est la combinaison d'un module de connexion compact et adaptable pour smartphone avec une application simple, intuitive et ergonomique afin de piloter son drone de loisirs.

Application de pilotage du drone

## Fonctionalités:
Design boitier de connexion  
Antenne de communication entre smartphone et drone  
Transfert de données du smartphone vers le drone  
Transfert de données du drone vers le smartphone  
IHM de pilotage en application smartphone  
Piloter le drone via l'IHM   

## User stories

### Design boitier de connexion
1 Etude d'aptabilité des connectiques entre Android et IOS x  
4 Design du boitier x  
2 Modélisation du boitier sur Solidworks x  
2 Choix des matériaux à l'aide de prototypes   
1 Choix du mode de production  
### Antenne de communication entre smartphone et drone
2 Réalisation d'un état de l'art sur les antennes pour drone  
1 Choix du type d'antenne   
2 Etude de l'intégration au boitier  
1 Intégration d'une antenne au prototype  
### Transfert de données du smartphone vers le drone
2 Réalisation d'un état de l'art x  
1 Choix du type de transfert du smartphone vers le drone x  
4 Créer lien entre IHM et antenne  
### Transfert de données du drone vers le smartphone
2 Réalisation d'un état de l'art x  
1 Choix du type de transfert du le drone vers le smartphone x  
4 Créer lien entre IHM et antenne  
### IHM de pilotage en application smartphone
2 Lister les fonctionnalitées que l'IHM doit fournir x  
8 Etude du design et Réalisation de maquettes pour l'IHM x  
4 Développement de l'IHM en Reactnative pour Android ( respectivement IOS )  
2 Adapter le code de l'application en reactnative pour IOS ( respectivement Android )  
### Piloter le drone via l'IHM
2 Réalisation d'un état de l'art sur le pilotage d'un drone via une application  
2 Intégrer mode statique à l'IHM  
2 Intégrer mode pilotage manuel  à l'IHM  
2 Intégrer mode tracking  à l'IHM  
2 Intégrer mode trajet préprogrammé  à l'IHM  
2 Intégrer mode retour maison  à l'IHM  


### Integrer objectif agile
Le projet reste stable à chaques étapes (test driven dev)  
L'quipe gère optimalement son temps (pomodoro)  
Les dev minimize leur erreurs (revue de code + pair programming)  
 
