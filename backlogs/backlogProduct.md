# Backlog du produit

## Fonctionalités:
Design boitier de connexion  
Antenne de communication entre smartphone et drone  
Transfert de données du smartphone vers le drone  
Transfert de données du drone vers le smartphone  
IHM de pilotage en application smartphone  
Piloter le drone  

## User stories

### Design boitier de connexion
Etude d'aptabilité des connectiques entre Android et IOS  
Modélisation du boitier sur Solidworks  
Choix des matériaux  
Réalisation d'un prototype  
Choix du mode de production  
### Antenne de communication entre smartphone et drone
Réalisation d'un état de l'art sur les antennes pour drone  
Choix du type d'antenne  
Etude de l'intégration au boitier  
Intégration d'une antenne au prototype  
### Transfert de données du smartphone vers le drone
Réalisation d'un état de l'art  
Choix du type de transfert du smartphone vers le drone  
Créer lien entre IHM et antenne  
### Transfert de données du drone vers le smartphone
Réalisation d'un état de l'art  
Choix du type de transfert du le drone vers le smartphone  
Créer lien entre IHM et antenne  
### IHM de pilotage en application smartphone
Lister les fonctionnalitées que l'IHM doit fournir  
Etude de l'ergonomie et du design de l'IHM  
Réalisation de maquette d'IHM  
Développement de l'IHM en Reactnative pour Android  
Développement de l'IHM en Reactnative pour IOS  
### Piloter le drone
Réalisation d'un état de l'art et recherche de bibliothèque  
Réaliser le modèle d'états du drone et des transitions possibles  
Créer mode statique  
Créer mode pilotage manuel  
Créer mode tracking  
Créer mode trajet préprogrammé  
Créer mode retour maison  
### Integrer objectif agile
Le projet reste stable à chaques étapes (test driven dev)  
L'quipe gère optimalement son temps (pomodoro)  
Les dev minimize leur erreurs (revue de code + pair programming)  
 
