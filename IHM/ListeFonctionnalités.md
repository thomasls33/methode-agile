# Liste des fonctionalités que IHM doit fournir


## Données à afficher

###Données de fonctionnement
Flux vidéo de la caméra  
Distance entre l'utilisateur et le drone  
Batterie restante du drone  
Batterie restante du smartphone  

###Données pilotages  
Altitude  
Coordonnées GPS  
Orientation actuelle part rapport au sol  
Vitesse de déplacement  


## Interactions possibles

### Contrôles de vol
#### Mode translations  
Joystick déplacements parrallèles au sol  
Joystick déplacement normaux au sol  
Joystick changement orientation  
#### Mode pilote (Similaire à jeu vidéo)  
Joystick déplacements  
Joystick changement orientation  

### Autres boutons  
Connexion/Déconnexion avec un drone  
Switch entre Statique/Manuel/Tracking/Parcours  
Switch entre les deux modes de pilotages manuel (à afficher uniquement en mode Manuel)  
Prendre photo  
Start/stop enregistrement vidéo  